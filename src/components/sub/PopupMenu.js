import React from 'react';
import {View,TouchableOpacity,UIManager,findNodeHandle} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const ICON_SIZE = 24;

class PopupMenu extends React.Component {
    handleShowPopupError = () => {
        // show error here
    };

    handleMenuPress = () => {
        const { actions, onPress } = this.props;

        UIManager.showPopupMenu(
            findNodeHandle(this.refs.viewOnWhichToPopup),
            ["Option 1", "Option 2"],
            () => console.log("something went wrong with the popup menu"),
            (e, i)=> console.log(e + " : " + i),
        );
    };

    render() {
        return (
            <View>
                { this.props.children }
                <TouchableOpacity onPress={this.handleMenuPress} style={{alignSelf:'center',backgroundColor:'transparent',paddingLeft:15,paddingRight:15}}>
                    <Icon
                        name="md-more"
                        size={ICON_SIZE}
                        color='white'
                        ref="menu"
                    />
                </TouchableOpacity>
            </View>
        );
    }
}
export default PopupMenu;