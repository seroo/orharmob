
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import Appbar from "./src/components/main/Appbar/Appbar";
import PopupMenu from './src/components/sub/PopupMenu'

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View >
        <Appbar > </Appbar>
        <PopupMenu actions={['item1', 'item2']} onPress={(e, i) => console.log(i)} > <Text >List İtem</Text> </PopupMenu>
      </View>
    );
  }
}


